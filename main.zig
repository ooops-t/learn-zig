//!参考文档: 
//!https://ziglearn.org/chapter-0/
//!https://ziglang.org/documentation/0.9.1/#Introduction

// 导入标准库"std"
const std = @import("std");

// 定义一个名为main，参数为空，返回值为'void'的函数
// 'main'函数为Zig程序的入口函数
pub fn main() anyerror!void {
    // 'std.log.info': 该函数用于输出日志
    // '{}': 占位符, '.{}': 占位符内容
    std.log.info("Hello, {s}", .{"World"});

    // 定义一个'i32'类型的常量，并且将其初始化为'5'
    // '@TypeOf': 该函数用于输出常量或变量的类型名
    const constant: i32 = 5;
    std.log.info("{} {}", .{ constant, @TypeOf(constant) });
    // 定义一个'u32'类型的变量，并将其初始化为'500'
    var variable: u32 = 5000;
    std.log.info("{} {}", .{ variable, @TypeOf(variable) });

    // '@as': 该函数用于强制类型转换
    // 将constant强制转换为'i64'类型
    const inferred_constant = @as(i64, constant);
    std.log.info("{} {}", .{ inferred_constant, @TypeOf(inferred_constant) });
    var inferred_variable = @as(u64, variable);
    std.log.info("{} {}", .{ inferred_variable, @TypeOf(inferred_variable) });

    // 如果一个变量或常量在定义时不能确定值的时候，可以将其初始化为'undefined'
    // 不过定义时需要说明相关的类型
    const a: i32 = undefined;
    std.log.info("{} {}", .{ a, @TypeOf(a) }); // 运行时是随机值
    var b: u32 = undefined;
    std.log.info("{} {}", .{ b, @TypeOf(b) });

    // 定义一个长度为5，元素类型为'u8'的常量数组
    const array = [5]u8{ 'H', 'e', 'l', 'l', 'o' };
    std.log.info("{} {}", .{ array.len, @TypeOf(array) });
    // array[0] = 'W'; // error: 使用'const'定义的数组元素不能被改变

    // 定义一个长度为5，元素类型为'u8'的变量数组
    var ar = [5]u8{ 'H', 'e', 'l', 'l', 'o' };
    std.log.info("{} {}", .{ ar.len, @TypeOf(ar) });
    ar[0] = 'W'; // 'var'定义的数组元素可以被改变

    // 定义运算类型为'u8'的数组，数组长度不关心
    // 编译器会自动推断出数组的长度
    const arl = [_]u8{ 'H', 'e', 'l', 'l', 'o' };
    std.log.info("{} {}", .{ arl.len, @TypeOf(arl) });

    // 'if': 判断的值只能为'bool'类型
    var x: u8 = 0;
    if (false) {
        x += 1;
    } else if (false) {
        x += 2;
    } else {
        x += 3;
    }
    std.log.info("{}", .{x});

    // 'if'作为表达式，将返回值赋值给变量
    // 语法挺怪，不能使用'{}'，不能使用表达式，不能使用';'
    var ifx: u16 = 0;
    ifx = if (true) 10 else 6;
    std.log.info("{}", .{ifx});

    // 'while'的':'后面表示一次循环结束后执行的语句, 主要用于循环次数控制
    var i: u16 = 1;
    while (i < 1000) : (i += 2) {
        i *= 2;
        if (i < 10)
            continue;

        if (i == 380)
            break;

        std.log.info("in: {}", .{i});
    }
    std.log.info("out: {}", .{i});

    const string = [_]u8{ 'a', 'b', 'c', 'd', 'e', 'f' };
    std.log.info("{} {}", .{ string.len, @TypeOf(string) });

    // 'for'循环可以一次性取出元素和对应的索引，如果那个不需要使用'_'代替就好
    for (string) |char, index| {
        std.log.info("{c} {}", .{ char, index });
    }

    // 定义函数
    std.log.info("addFive(10): {}", .{addFive(10)});
    std.log.info("fibonacci(10): {}", .{fibonacci(10)});

    var deferv: i16 = 5;
    {
        // 使用'defer'修饰的表达式，只有在该语句块退出时才会执行
        defer deferv += 4;
        std.log.info("deferv: {}", .{deferv});
    }
    std.log.info("deferv: {}", .{deferv});

    deferv = 0;
    {
        // 使用'defer'修饰的表达式，只有在该语句块退出时才会执行
        // 如果一个语句块中有多个'defer'修饰的表达式，将会从后往前执行
        defer deferv += 4;
        deferv += 3;
        std.log.info("deferv: {}", .{deferv});
        defer deferv = 0;
    }
    std.log.info("deferv: {}", .{deferv});

    // error类型，全局通用的错误anyerror
    const FileOpenError = error{
        AccessDenied,
        OutOfMemory,
        FileNotFound,
    };

    const AllocationError = error{OutOfMemory};

    const err: FileOpenError = AllocationError.OutOfMemory;
    std.log.info("{}", .{err == FileOpenError.OutOfMemory});

    // error类型和通用类型使用'!'结合为一个可选择类型
    // 以'maybe_error'为例，也就是说该变量的值要么是error，要么是u16
    const maybe_error: AllocationError!u16 = 10;
    const no_error = maybe_error catch 0;
    std.log.info("{}", .{@TypeOf(maybe_error)});
    std.log.info("{}", .{u16 == @TypeOf(no_error)});
    std.log.info("{}", .{no_error == 10});

    // catch用户捕获error类型，并传出错误值
    //    failingFunction() catch |cerr| {
    //        std.log.info("{}", .{cerr});
    //    };

    // var ret = isFailed(true) catch |ierr| {
    //     return ierr;
    // };
    //
    // try是catch语法的一个语法糖，捕获到error直接传入上层函数
    var ret = try isFailed(false);
    std.log.info("{}", .{ret});

    const A = error{ NotDir, PathNotFound };
    const B = error{ OutOfMemory, PathNotFound };
    const C = A || B;
    std.log.info("{}", .{C});

    // switch
    var sw: i8 = 10;
    switch (sw) {
        -1...1 => {
            sw = -sw;
        },
        10, 100 => {
            sw = @divExact(sw, 10);
        },
        else => {
            std.log.info("No match", .{});
        },
    }
    std.log.info("sw: {}", .{sw});

    sw = switch (sw) {
        -1...1 => -sw,
        10, 100 => @divExact(sw, 10),
        else => sw,
    };
    std.log.info("sw: {}", .{sw});

    {
        @setRuntimeSafety(false);
        const aa = [3]u8{ 1, 2, 3 };
        var index: u8 = 5;
        const bb = aa[index]; // 该操作会出现内存越界访问的错误，如果加上'@setRuntimeSafety(false)'则不会产生错误，直接使该值为0
        std.log.info("bb: {}", .{bb});
    }

    // Unreachable
    // 说明该分支永远不可能到达，如果到达则会产生错误
    const u: i32 = 1;
    const y: u32 = if (u == 1) 5 else unreachable;
    std.log.info("u: {}", .{y});

    // 'switch'中也可以使用'unreachable'
    std.log.info("a to upper: {c}", .{asciiToUpper('a')});

    // Pointers
    var v: u8 = 1;
    const pv: *u8 = &v; // '&': 取地址
    std.log.info("v: {}, pv: {}", .{ v, pv.* }); // '.*': 取出该地址上的值
    std.log.info("pointer size: {}, isize: {}", .{ @sizeOf(*u8), @sizeOf(isize) });
    std.log.info("pointer size: {}, usize: {}", .{ @sizeOf(*u8), @sizeOf(usize) });

    // Slices
    const sarray = [_]u8{ 1, 2, 3, 4, 5 };
    const slice = sarray[0..3];
    std.log.info("slices type: {}", .{@TypeOf(slice)}); // *const [3]u8
    printArrayU8(slice); // 1, 2, 3

    // enum
    const Value = enum(u2) { zero, one, two };
    std.log.info("enum: {}", .{Value});

    const Values = enum(u32) {
        hundred = 100,
        thousand = 1000,
        million = 1000000,
        next,
    };
    std.log.info("next: {}", .{@enumToInt(Values.next)});

    std.log.info("Suit clubs: {}", .{Suit.getClubs()});
    Suit.setClubs(111);
    std.log.info("Suit clubs: {}", .{Suit.getClubs()});

    // struct
    var thing = MyVec{ .x = 10, .y = 20 };
    thing.swap();
    std.log.info("thing x: {} y: {}", .{ thing.x, thing.y });
}

const Suit = enum {
    var clubs: u8 = 10;
    spades,
    diamonds,
    hearts,

    pub fn isClubs(self: Suit) bool {
        return self == Suit.clubs;
    }
    pub fn getClubs() u8 {
        return Suit.clubs;
    }
    pub fn setClubs(v: u8) void {
        Suit.clubs = v;
    }
};

const MyVec = struct {
    x: i32,
    y: i32,
    z: f32 = 0, // default value
    w: f32 = undefined,
    fn swap(self: *MyVec) void {
        const tmp = self.x;
        self.x = self.y;
        self.y = tmp;
    }
};

// 定义参数为u32类型，返回值为u32类型，名称为'addFive'的函数
fn addFive(x: u32) u32 {
    return x + 5; // 返回值
}

// 递归调用函数
fn fibonacci(n: u16) u16 {
    if (n == 0 or n == 1) return n;
    return fibonacci(n - 1) + fibonacci(n - 2);
}

fn failingFunction() error{Oops}!void {
    return error.Oops;
}

fn isFailed(x: bool) error{Oops}!u8 {
    return if (x) error.Oops else 10;
}

fn asciiToUpper(x: u8) u8 {
    return switch (x) {
        'a'...'z' => x + 'A' - 'a',
        'A'...'Z' => x,
        else => unreachable,
    };
}

fn printArrayU8(array: []const u8) void {
    for (array) |n, in| {
        std.log.info("number: {}, index: {}", .{ n, in });
    }
}
